<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

class JBText
{	
	public static $translation = array();
	
	public $file;
	
	function __construct($file)
	{
		$this->file = $file;
	}
	
	public function _($text)
	{
		if(isset(self::$translation[$this->file][$text]))
		{
			return self::$translation[$this->file][$text];
		}
		
		return $text;		
	}

	public static function load($extype, $exname)
	{
		$cfg = App::getConfig();
		$language = $cfg->language;
		
		// added by Vicky - read user session to get their language
		$ses = App::getSession();
		$language = $ses->get("language");
		
		// replace language if set by cookie
		if(isset($_COOKIE["bahasa"]))
		{
			$language = $_COOKIE["bahasa"];
		}	 
		 
		$file = JBAPPS."/".$extype."/".$exname."/language/".$language.".json";
		
		//klo bahasa yang dipilih tidak ada maka sesuai config default
		if(!file_exists($file))
		{
			$file = JBAPPS."/".$extype."/".$exname."/language/".$cfg->language.".json";
		}
		
		//klo file belum ada di static translation
		if(!isset(self::$translation[$file]))
		{
			if(file_exists($file))
			{
				$isi = file_get_contents($file);
				$json = json_decode($isi, true);
				self::$translation[$file] = $json;
			}			
		}

		return new JBText($file);
	}
}
