<?php
/* Load all framework components
(C) Vicky */
// No direct access to this file
defined('direct') or die;

require_once __DIR__."/app.php";
require_once __DIR__."/helper.php";
require_once __DIR__."/library/library.php";
require_once __DIR__."/database/idatabase.php";
require_once __DIR__."/input/jinput.php";
require_once __DIR__."/session/jbsessionstorage.php";
require_once __DIR__."/session/isession.php";
require_once __DIR__."/language/jbtext.php";
require_once __DIR__."/document/controller.php";
require_once __DIR__."/user/user.php";
require_once __DIR__."/event/events.php";
require_once __DIR__."/router/router.php";
require_once __DIR__."/document/modules.php";
require_once __DIR__."/document/view.php";
require_once __DIR__."/document/plugin.php";

?>