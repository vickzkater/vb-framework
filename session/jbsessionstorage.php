<?php
/* VB Framework
 * For managing session storage
 * (C) Vicky
 */
 
// No direct access to this file
defined('direct') or die;

class JBSessionStorage
{	
	public $lifetime = 3600;

	public function open($path, $name)
	{
		return true;
	}
	
	public function close()
	{
		return true;
	}
	
	public function read($id)
	{
		$db = App::getDbo();
		$sql = "SELECT data FROM sessions WHERE id = ".$db->quote($id);
		$db->setQuery($sql);
        $results = $db->loadObjectList();
		if(count($results) > 0){
			foreach($results as $r)
			{
				return $r->data;
			}
		}
		 
		return "";
	}
	
    public function write($id, $data)
	{
        // new session-expire-time
		$db = App::getDbo();
        $newExp = time() + $this->lifetime;
       
		$userid = 0;
		if(isset($_SESSION['userid']))
		{
			$userid = (int)$_SESSION['userid'];
		}
	   
		$sql = "INSERT INTO sessions (id, expires, data, userid) 
				VALUES (".$db->quote($id).", ".$db->quote($newExp).", ".$db->quote($data).", ".$db->quote($userid).")
				ON DUPLICATE KEY UPDATE data = ".$db->quote($data).", expires = ".$db->quote($newExp).", userid = ".$db->quote($userid);
				
		$db->setQuery($sql);
		
		//call garbage collector..
		$this->gc();
	
		return $db->result;
    }

    public function destroy($id)
	{
        // delete session-data
		$db = App::getDbo();
        $sql = "DELETE FROM sessions WHERE id = ".$db->quote($id);

        $db->setQuery($sql);

		return $db->result;
    } 
	
    public function gc($lifetime = 18000)
	{
		$db = App::getDbo();
		
		// delete sessions userid = 0 - added by Vicky 2018-10-04 for delete unknown sessions created by server
		$db->setQuery('DELETE FROM sessions WHERE userid = 0 AND (`data` = "" OR `data` LIKE '.$db->like('N;').')');
		
        // delete old sessions
		$old = time() - $lifetime;
		$sql = "DELETE FROM sessions WHERE expires < ".$old;
		$db->setQuery($sql);
		return $db->getAffectedRows();
    } 
	
}
