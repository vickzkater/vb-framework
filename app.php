<?php

/* App | VB Framework
 * (C) Vicky
 */
// No direct access to this file
defined('direct') or die;

class App {
    /*
     * config singleton
     */

    public static $config = null;
    public static $time = null;

    public static function getTimeStart($microtime = null) {
        if (!self::$time) {
            if (!$microtime) {
                $microtime = microtime(true);
            }
            self::$time = $microtime;
        }
        return self::$time;
    }

    public static function getUser($id = null) {
        return JBUser::getInstance($id);
    }

    public static function getConfig() {
        if (!self::$config) {
            self::$config = new Config();
        }
        return self::$config;
    }

    public static function getDbo() {
        return IDatabase::getInstance();
    }

    public static function getSession() {
        return ISession::getInstance();
    }

    public static function getLibrary() {
        return Library::getInstance();
    }

    public static function jsLibrary($name) {
        $libs = self::getLibrary();
        $js = $libs->getJS($name);
        return $js;
    }

    public static function execute() {
        $cfg = App::getConfig();

        $route = true;

        if ($cfg->sef) {
            $route = IRoute::parseUri($_GET);
        }

        //get homepage component
        $home = $cfg->routing[""];
        $component = Jinput::get('option', $home, 'alnum');

        $path = JBAPPS . '/components/' . $component . '/' . $component . '.php';
        if (file_exists($path) && $route) {
            require_once $path;
        } else {
            $path = JBAPPS . "/templates/" . $cfg->template . "/404.php";
            require_once $path;
        }
    }

    public static function message($message, $type = 'success') {
        $ses = App::getSession();
        $messages = $ses->get('message');
        if (!$messages) {
            $messages = array();
        }

        $mes = array('message' => $message, 'type' => $type);
        $messages[] = $mes;
        $ses->set('message', $messages);
    }

    public static function getMessage() {
        $ses = App::getSession();
        $messages = $ses->get('message');
        if (isset($messages)) {
            if (is_array($messages)) {
                return $messages;
            }
        } else {
            return array();
        }
    }

    public static function clearMessage() {
        $ses = App::getSession();
        $ses->remove('message');
    }

    public static function redirect($url = null, $own = true) {
        if (!$url) {
            $url = JBSITE . "/";
        } else if (strpos($url, JBURI) === false && $own) {
            $url = JBSITE . "/" . $url;
        }

        $format = Jinput::get('format', null, 'alnum');
        $option = Jinput::get('option', null, 'alnum');

        if ($option == 'ajax' || $format == 'pjax') {
            echo '<script type="text/javascript">document.location.href="' . $url . '";</script>';
            die;
        }

        if (!headers_sent()) {
            header('HTTP/1.1 303 See other');
            header('Location: ' . $url);
        } else {
            echo '<script type="text/javascript">document.location.href="' . $url . '";</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
            echo '</noscript>';
        }

        die;
    }

    public static function getLayout($com, $view, $layout) {
        // Mulai output Buffer
        ob_start();

        // Muat halaman layout
        include JBAPPS . '/components/' . $com . '/views/' . $view . '/tmpl/' . $layout . '.php';

        // get halaman contentnya
        $output = ob_get_clean();

        return $output;
    }

    private function sendMailProcess($mail, $subject, $message, $to, $from, $replyto, $cc, $bcc, $file, $silent) {
        $cfg = $this->getConfig();
        try {
            //Set PHPMailer to use the sendmail transport
            $mail->isSendmail();
            //Set who the message is to be sent from
            if ($from) {
                $mail->setFrom($from->email, $from->name);
            } else {
                $mail->setFrom($cfg->SMTPFromEmail, $cfg->SMTPFromName);
            }
            //Set an alternative reply-to address
            if ($replyto) {
                $mail->addReplyTo($replyto->email, $replyto->name);
            }
            //Set CC
            if ($cc) {
                $mail->addCC($cc->email, $cc->name);
            }
            //Set BCC
            if ($bcc) {
                $mail->addBCC($bcc->email, $bcc->name);
            }
            //Set who the message is to be sent to
            $mail->addAddress($to->email, $to->name);
            //Set the subject line
            $mail->Subject = $subject;
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $mail->msgHTML($message);
            //Replace the plain text body with one created manually
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
            //Add attachment file pdf
            if ($file) {
                $mail->addAttachment($file);
            }
            //send mail
            return $mail->send();
        } catch (phpmailerException $e) {
            if (!$silent) {
                App::message(strip_tags($e->errorMessage()), 'error');
            } return false;
        } catch (Exception $e) {
            if (!$silent) {
                App::message(strip_tags($e->errorMessage()), 'error');
            } return false;
        }
    }

    public static function sendMail($subject, $message, $to, $from, $replyto = null, $cc = null, $bcc = null, $file = null, $silent = true) {
        require_once JBLIBRARY . "/email/phpmailer/PHPMailerAutoload.php";

        $cfg = App::getConfig();

        //Create a new PHPMailer instance
        $mail = new PHPMailer;

        //Enable SMTP debugging. 
        $mail->SMTPDebug = $cfg->SMTPDebug;
        //Set PHPMailer to use SMTP.
        $mail->isSMTP();
        //Set SMTP host name                          
        $mail->Host = $cfg->SMTPHost;
        //Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = $cfg->SMTPAuth;
        //Provide username and password     
        $mail->Username = $cfg->SMTPUsername;
        $mail->Password = $cfg->SMTPPassword;
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = $cfg->SMTPSecure;
        //Set TCP port to connect to 
        $mail->Port = $cfg->SMTPPort;

        return $this->sendMailProcess($mail, $subject, $message, $to, $from, $replyto, $cc, $bcc, $file, $silent);
    }

    public static function parseEmailTemplate($data, $html) {
        $output = $html;

        foreach ($data as $k => $v) {
            $output = str_replace("[" . $k . "]", $v, $output);
        }

        return $output;
    }

    public static function getAllFolder($dir) {
        $hasil = array();
        if (file_exists($dir)) {
            $folder = scandir($dir, 1);
            if (count($folder) > 0) {
                foreach ($folder as $k => $v) {
                    if (!is_file($dir . '/' . $v) && $v != '..' && $v != '.') {
                        $hasil[] = $v;
                    }
                }
            }
        }
        return $hasil;
    }

    public static function close($num = 0) {
        exit($num);
    }

    public static function fourohfour() {
        App::redirect('?page=404');
    }

}
