<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;
?>
<script type="text/javascript">
$(document).on("click", ".parentSel", function(){
    if($('.parentSel').is(':checked'))
    {
        $(".childSel").prop('checked', true);
    }else{
        $(".childSel").prop('checked', false);
    }
});
</script> 