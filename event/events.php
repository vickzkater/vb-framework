<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

class Events {

	// Plugin Group : Authentication
	public static function onSessionCheck()
	{
		$group = 'authentication';
		$plugins = App::getAllFolder(JBPLUGIN.'/'.$group);
		if(count($plugins) > 0){
			foreach($plugins as $u => $p){
				$pluginPath = JBPLUGIN."/".$group."/".$p."/".$p.".php";
				if(file_exists($pluginPath))
				{			
					require_once $pluginPath;
					$pluginClass = ucfirst($p)."Plugin";					
					if(class_exists($pluginClass))
					{
						if(!method_exists($pluginClass,'onSessionCheck'))
						{
							continue;
						}
						$plugin = new $pluginClass();
						if($plugin->active == 1)
						{
							$plugin->onSessionCheck();
						}
					}else{
						continue;
					}
				}else{
					continue;
				}
			}
		}
		return false;	
	}
	
	public static function onLoginProcess()
	{
		$group = 'authentication';
		$plugins = App::getAllFolder(JBPLUGIN.'/'.$group);
		if(count($plugins) > 0){
			foreach($plugins as $u => $p){
				$pluginPath = JBPLUGIN."/".$group."/".$p."/".$p.".php";
				if(file_exists($pluginPath))
				{			
					require_once $pluginPath;
					$pluginClass = ucfirst($p)."Plugin";					
					if(class_exists($pluginClass))
					{
						if(!method_exists($pluginClass,'onLoginProcess'))
						{
							continue;
						}
						$plugin = new $pluginClass();
						if($plugin->active == 1)
						{
							$plugin->onLoginProcess();
						}
					}else{
						continue;
					}
				}else{
					continue;
				}
			}
		}
		return false;	
	}

	public static function onAffiliateBeforeUpdate($data)
	{
		$brokers = Brokers::getAll();
		$valid = true;
		if(count($brokers > 0)){
			foreach($brokers as $b){
				$brokerPath = IROOT."/library/brokers/".$b->bname.".php";
				
				/*
				 * Jika sudah tidak valid maka keluarkan dari looping
				 * tidak perlu mengecek ke broker lain..
				 */
				if(!$valid){
					return false;
					break;
				}
				if(file_exists($brokerPath))
				{			
					require_once $brokerPath;					
					if(class_exists($b->bname))
					{
						$brokerClass = ucfirst($b->bname);
						$broker = new $brokerClass($data->user->id);
						$valid = $broker->onAffiliateBeforeUpdate($data);
					}else{
						$message = $b->bname." class is not same with filename";
						App::message($message);
						$valid = false;
						return false;
					}
				}else{
					$message = $b->bname." event trigger file not found";
					App::message($message);
					$valid = false;	
					return false;
				}
			}
		}
		return $valid;
	}

	public static function onAffiliateUpdate($data, &$success, &$pending)
	{
		$brokers = Brokers::getAll();
		$valid = 0;
		if(count($brokers > 0)){
			foreach($brokers as $b){
				$brokerPath = IROOT."/library/brokers/".$b->bname.".php";
	
				if(file_exists($brokerPath))
				{			
					require_once $brokerPath;					
					if(class_exists($b->bname))
					{
						$brokerClass = ucfirst($b->bname);
						$broker = new $brokerClass($data->user->id);
						$valid = $broker->onAffiliateUpdate($data, $success, $pending);
					}else{
						$message = $b->bname." class is not same with filename";
						App::message($message);
						$valid = 0;
					}
				}else{
					$message = $b->bname." event trigger file not found";
					App::message($message);
					$valid = 0;
				}
			}
		}
		return $valid;
	}
	
	public static function onFormUpdateAffiliate($userid)
	{
		$brokers = Brokers::getAll();
		if(count($brokers > 0)){
			foreach($brokers as $b){
				$brokerPath = IROOT."/library/brokers/".$b->bname.".php";
				if(file_exists($brokerPath))
				{
					require_once $brokerPath;
					if(class_exists($b->bname))
					{
						$brokerClass = ucfirst($b->bname);
						$broker = new $brokerClass($userid);
						$broker->onFormUpdateAffiliate();
					}else{
						echo $b->bname." class file not found";
						die;
						return false;
					}
				}else{
					echo $b->bname." form function not found";
					die;
					return false;
				}
			}
		}
	}
	
	public static function onAffiliateAfterUpdate($success, $pending)
	{
		if(count($success) > 0)
		{
			foreach($success as $s){
				$brokerPath = IROOT."/library/brokers/".$s->broker.".php";
				if(file_exists($brokerPath))
				{
					require_once $brokerPath;
					if(class_exists($s->broker))
					{
						$brokerClass = ucfirst($s->broker);
						$broker = new $brokerClass($s->userid);
						$valid = $broker->onAffiliateAfterUpdateSuccess($s->userid, $s->data, $s->response);
					}else{
						$message = "Error in Success Action onAffiliateAfterUpdate ".$s->broker." class is not same with filename";
						App::message($message);
						$valid = 0;
					}
				}else{
					$message = "Error in Success Action onAffiliateAfterUpdate ".$s->broker." event trigger file not found";
					App::message($message);
					$valid = 0;
				}
			}			
		}
		
		if(count($pending) > 0)
		{
			foreach($pending as $p){
				$brokerPath = IROOT."/library/brokers/".$p->broker.".php";
				if(file_exists($brokerPath))
				{
					require_once $brokerPath;
					if(class_exists($p->broker))
					{
						$brokerClass = ucfirst($p->broker);
						$broker = new $brokerClass($p->userid);
						$valid = $broker->onAffiliateAfterUpdatePending($p->userid, $p->data, $p->task);
					}else{
						$message = "Error in Pending Action onAffiliateAfterUpdate ".$p->broker." class is not same with filename";
						App::message($message);
						$valid = 0;
					}
				}else{
					$message = "Error in Pending Action onAffiliateAfterUpdate ".$p->broker." event trigger file not found";
					App::message($message);
					$valid = 0;
				}
			}			
		}		
	}
	
	public static function onMemberOpenBroker()
	{	
		$user = App::getUser();
		
		$html = '';
		$brokers = Brokers::getAll();
		$valid = true;
		if(count($brokers > 0)){
			foreach($brokers as $b){
				$brokerPath = IROOT."/library/brokers/".$b->bname.".php";
				if(file_exists($brokerPath))
				{
					require_once $brokerPath;
					if(class_exists($b->bname))
					{
						$brokerClass = ucfirst($b->bname);
						$broker = new $brokerClass($user->id);
						$html .= $broker->onMemberOpenBroker();
					}
				}
			}
		}
		return $html;
	}

	public static function onMemberSidebar()
	{	
		$html = '';
		$brokers = Brokers::getAll();
		$valid = true;
		if(count($brokers > 0)){
			foreach($brokers as $b){
				$brokerPath = IROOT."/library/brokers/".$b->bname.".php";
				if(file_exists($brokerPath))
				{
					require_once $brokerPath;
					if(class_exists($b->bname))
					{
						$brokerClass = ucfirst($b->bname);
						$broker = new $brokerClass();
						$html .= $broker->onMemberSidebar();
					}
				}
			}
		}
		return $html;
	}	
}