<?php

/* JoyBoy Framework
  (C) Jiboy */
// No direct access to this file
defined('direct') or die;

class IRoute {

    public static function _($url) {
        $cfg = App::getConfig();
        $com = Jinput::get('option', $cfg->component, 'string');

        $query = array();
        $segments = array();

        $uri = $url;

        $url2 = explode('?', $url);
        if (count($url2) > 1) {
            $url3 = explode('&', $url2[1]);
            foreach ($url3 as $k) {
                $val = null;
                $url4 = explode('=', $k);
                $name = $url4[0];
                if (isset($url4[1])) {
                    $val = $url4[1];
                }
                if ($name == 'option' && isset($val)) {
                    $com = $val;
                }
                if (isset($val)) {
                    $query[$name] = $val;
                }
            }
            $path = JBAPPS . '/components/' . $com . '/router.php';
            if (!file_exists($path)) {
                return "/" . $url;
            } elseif (!$cfg->sef) {
                return "/" . $url;
            } else {
                require_once $path;
            }

            $rt = $com . 'BuildRoute';

            $segments = $rt($query);
            $uri = JBSITE . "/";
            foreach ($segments as $s) {
                if ($s != "") {
                    $uri .= $s . '/';
                }
            }

            if (count($query) > 0) {
                $uri .= '?';
                foreach ($query as $n => $v) {
                    $uri .= $n . '=' . $v . '&';
                }
                $uri = substr($uri, 0, -1);
            }
        }

        return $uri;
    }

    public static function parseUri(&$get) {
        //tampung $_GET sementara
        $temp = $_GET;

        $cfg = App::getConfig();

        // cek apa ada character ? untuk get request
        $url = self::fullUrl($_SERVER);
        $arr = explode(JBSITE, $url);
        if(count($arr) > 0){
            if(isset($arr[1])){
                $url = $arr[1];
            }
        }

        $url = explode('?', $url);
        $command = explode('/', $url[0]);

        $segments = array();

        foreach ($command as $k => $v) {
            if ($v != "") {
                $segments[] = $v;
            }
        }

        if (count($segments) < 1) {
            $segments[0] = '';
        }

        /*
         * karena disini tidak ada menu yang terintegrasi dengan database
         * maka di hardcoded disini untuk routing url
         * menggunakan setting masing-masing component router
         */

        //jika segment pertama adalah user maka gunakan user route

        $component = null;

        //jika sef = true..
        if ($cfg->sef) {
            $alias = $cfg->routing;

            if (array_key_exists($segments[0], $alias)) {
                $component = $alias[$segments[0]];
            }

            $path = JBAPPS . '/components/' . $component . '/router.php';
            if ($component) {
                if (file_exists($path)) {
                    require_once $path;
                    $function = ucfirst($component) . 'ParseRoute';
                    $vars = $function($segments);

                    foreach ($vars as $k => $v) {
                        //jika key tidak ada di get uri maka override..
                        if (!array_key_exists($k, $temp)) {
                            $get[$k] = $v;
                        }
                    }
                }
            }
        }

        return true;
    }

    /*
     * function to disjoint uri
     * digunakan untuk pjax
     */

    public static function disjointUri($url) {

        // cek apa ada character ? untuk get request
        $arr = explode(JBSITE, $url);
        $url = $arr[1];

        $url = explode('?', $url);
        $command = explode('/', $url[0]);
        $segments = array();

        foreach ($command as $k => $v) {
            if ($v != "") {
                $segments[] = $v;
            }
        }

        if (count($segments) < 1) {
            $segments[0] = '';
        }

        /*
         * karena disini tidak ada menu yang terintegrasi dengan database
         * maka di hardcoded disini untuk routing url
         * menggunakan setting masing-masing component router
         */

        //jika segment pertama adalah user maka gunakan user route

        $component = null;

        $alias = array("" => "office", "ajax" => "ajax", "pjax" => "ajax");

        if (array_key_exists($segments[0], $alias)) {
            $component = $alias[$segments[0]];
        }

        $path = JBAPPS . '/components/' . $component . '/router.php';

        if ($component) {
            if (file_exists($path)) {
                require_once $path;
                $function = ucfirst($component) . 'ParseRoute';
                $vars = $function($segments);
                foreach ($vars as $k => $v) {
                    //jika key tidak ada di get uri maka override..
                    if (!array_key_exists($k, $temp)) {
                        $get[$k] = $v;
                    }
                }
            }
        }
        return $get;
    }

    public static function setRouting($get, $value) {
        if (isset(JBRegistry::$routing[$get])) {
            JBRegistry::$routing[$get] = $value;
        }
    }

    private static function urlOrigin($s, $use_forwarded_host = false) {
        $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $s['SERVER_PORT'];
        $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
        $host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
        $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
        return $protocol . '://' . $host;
    }

    private static function fullUrl($s, $use_forwarded_host = false) {
        return self::urlOrigin($s, $use_forwarded_host) . $s['REQUEST_URI'];
    }

}

?>
