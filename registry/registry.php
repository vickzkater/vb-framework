<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

class JBRegistry {

	public static $routing = array();
	
	public static $log  = array();
	
	public static function setLog($message)
	{
		self::$log[] = $message;
	}

}