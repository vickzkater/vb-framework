<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;
?>
<script type="text/javascript">
function pjax(url, parameter)
{
	var dataString = parameter;
	return $.ajax({
		type: "GET",
		dataType: "json",
		data: dataString,
		beforeSend: function(x) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json;charset=UTF-8");
			}
		},
		url: url,
		cache: false,
		timeout: 300000, // 5 minutes
		success: function(result){
			
		},
		error: function(resp, status, error){
			if(status=="timeout"){
				console.log("Disconnected. Please check your network connection");
				if($("#box-content").find(".ajax-status").length){
					$(".ajax-status").html("<h3 class='text-center' style='color:red'><i class='fa fa-times'></i> &nbsp;Content Request Failed! Please Try Again..<br><br></h3>");
				}else{
					$("#box-content").prepend("<div class='overlay ajax-status'></div>");
					$(".ajax-status").prepend("<h3 class='text-center' style='color:red'><i class='fa fa-times'></i> &nbsp;Content Request Failed! Please Try Again..<br><br></h3>");
				}
			}else{
				$( "body").append( "<div class=\"hidden-js-error\"></div>" );
				$( ".hidden-js-error" ).hide();
				$( ".hidden-js-error" ).prepend(resp.responseText);
				console.log("Error : "+ error);
				if($("#box-content").find(".ajax-status").length){
					$(".ajax-status").html("<h3 class='text-center' style='color:red'><i class='fa fa-times'></i> &nbsp;Content Request Failed! Please Try Again..<br><br></h3>");
				}else{
					$("#box-content").prepend("<div class='overlay ajax-status'></div>");
					$(".ajax-status").prepend("<h3 class='text-center' style='color:red'><i class='fa fa-times'></i> &nbsp;Content Request Failed! Please Try Again..<br><br></h3>");
				}
			}
		}
	});
}
</script>