<?php

/* VB Framework
  (C) Vicky */
// No direct access to this file
defined('direct') or die;

class Jinput {

    public static function get($cmd, $default = null, $type) {
        $a = $default;
        if (isset($_GET[$cmd])) {
            if ($type == 'string') {
                $a = $_GET[$cmd];
                $a = Jinput::getString($a, $default);
            }
            if ($type == 'int') {
                $a = $_GET[$cmd];
                $a = Jinput::getInt($a, $default);
            }
            if ($type == 'number') {
                $a = $_GET[$cmd];
                $a = Jinput::getNumber($a, $default);
            }
            if ($type == 'double') {
                $a = $_GET[$cmd];
                $a = Jinput::getDouble($a, $default);
            }
            if ($type == 'text') {
                $a = $_GET[$cmd];
                $a = Jinput::getText($a, $default);
            }
            if ($type == 'word') {
                $a = $_GET[$cmd];
                $a = Jinput::getWord($a);
            }
            if ($type == 'alnum') {
                $a = $_GET[$cmd];
                $a = Jinput::getAlnum($a, $default);
            }
            if ($type == 'email') {
                $a = $_GET[$cmd];
                $a = Jinput::getEmail($a);
            }
            if ($type == 'raw') {
                $a = $_GET[$cmd];
                $a = Jinput::getRaw($a);
            }
            if ($type == 'name') {
                $a = $_GET[$cmd];
                $a = Jinput::getName($a, $default);
            }
        }

        return $a;
    }

    public static function post($cmd, $default = null, $type) {
        $a = $default;
        if (isset($_POST[$cmd])) {
            if ($type == 'string') {
                $a = $_POST[$cmd];
                $a = Jinput::getString($a, $default);
            }
            if ($type == 'int') {
                $a = $_POST[$cmd];
                $a = Jinput::getInt($a, $default);
            }
            if ($type == 'number') {
                $a = $_POST[$cmd];
                $a = Jinput::getNumber($a, $default);
            }
            if ($type == 'double') {
                $a = $_POST[$cmd];
                $a = Jinput::getDouble($a, $default);
            }
            if ($type == 'text') {
                $a = $_POST[$cmd];
                $a = Jinput::getText($a, $default);
            }
            if ($type == 'word') {
                $a = $_POST[$cmd];
                $a = Jinput::getWord($a);
            }
            if ($type == 'alnum') {
                $a = $_POST[$cmd];
                $a = Jinput::getAlnum($a, $default);
            }
            if ($type == 'email') {
                $a = $_POST[$cmd];
                $a = Jinput::getEmail($a);
            }
            if ($type == 'array') {
                $a = $_POST[$cmd];
                $a = Jinput::getArray($a);
            }
            if ($type == 'raw') {
                $a = $_POST[$cmd];
                $a = Jinput::getRaw($a, $default);
            }
            if ($type == 'name') {
                $a = $_POST[$cmd];
                $a = Jinput::getName($a, $default);
            }
            if ($type == 'files') {
                $a = Jinput::files($cmd, $default);
            }
        }

        return $a;
    }

    public static function request($cmd, $default = null, $type) {
        if (isset($_REQUEST[$cmd])) {
            if ($type == 'string') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getString($a, $default);
            }
            if ($type == 'int') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getInt($a, $default);
            }
            if ($type == 'number') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getNumber($a, $default);
            }
            if ($type == 'text') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getText($a, $default);
            }
            if ($type == 'word') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getWord($a);
            }
            if ($type == 'alnum') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getAlnum($a, $default);
            }
            if ($type == 'email') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getEmail($a);
            }
            if ($type == 'array') {
                $a = $_REQUEST[$cmd];
                $a = Jinput::getArray($a);
            }
        } else {
            $a = $default;
        }
        return $a;
    }

    public static function files($cmd, $default = null) {
        if (isset($_FILES[$cmd])) {
            if ($_FILES[$cmd]['tmp_name'] == "") {
                return $default;
            } else {
                return $_FILES[$cmd];
            }
        } else {
            return $default;
        }
    }

    public static function set($name, $value) {
        $_REQUEST[$name] = $value;
    }

    public static function getWord($string) {
        $a = strtolower(preg_replace("/[^a-z]+/i", "", $string));
        return $a;
    }

    public static function getAlnum($string, $default) {
        if (ctype_alnum($string)) {
            return $string;
        } else {
            return $default;
        }
    }

    /**
     * only allow alphabet (a-z/A-Z) and symbols (whitespace and underscore)
     * @param type $string
     * @param type $default
     * @return type
     */
    public static function getName($string, $default) {
        if ($string == '' || !$string) {
            return $default;
        }
        $val = preg_replace("/[^ \w]+/", "", $string);
        return $val;
    }

    /**
     * only allow integer
     * @param type $string
     * @param type $default
     * @return type
     */
    public static function getInt($string, $default = 0) {
        $val = filter_var($string, FILTER_VALIDATE_INT);
        if ($val !== false) {
            return (int) $string;
        }
        return $default;
    }

    /**
     * only allow numeric (0-9) and double/float value
     * @param type $string
     * @param type $default
     * @return type
     */
    public static function getDouble($string, $default = 0) {
        $matches = NULL;
        // Only use the first floating point value
        preg_match('/-?[0-9]+(\.[0-9]+)?/', (string) $string, $matches);
        $result = (float) $matches[0];

        if ($result) {
            return $result;
        }
        return $default;
    }

    /**
     * only allow email value
     * @param type $string
     * @return type
     */
    public static function getEmail($string) {
        $val = filter_var($string, FILTER_VALIDATE_EMAIL);
        if ($val) {
            return $string;
        }
        return null;
    }

    /**
     * only allow array value
     * @param type $array
     * @return type
     */
    public static function getArray($array) {
        if (is_array($array)) {
            return $array;
        }
        return null;
    }

    /**
     * only allow numeric (0-9), symbols not allowed
     * @param type $string
     * @param type $default
     * @return type
     */
    public static function getNumber($string, $default) {
        if ($string == '' || !$string) {
            return $default;
        }
        return preg_replace('/[^0-9]/', '', $string);
    }

    /**
     * allow all characters within "FILTER_SANITIZE_MAGIC_QUOTES"
     * remove symbol backslash (\) before single quote (') and double quote (")
     * @param type $string
     * @param type $default
     * @return type
     */
    public static function getString($string, $default) {
        if ($string == '' || !$string) {
            return $default;
        }
        $val = filter_var($string, FILTER_SANITIZE_MAGIC_QUOTES);
        return stripslashes($val);
    }

    /**
     * allow all characters within "FILTER_SANITIZE_MAGIC_QUOTES"
     * if it contains symbols: single quote (') and double quote (") then it adds symbol backslash (\) before those symbols
     * @param type $string
     * @param type $default
     * @return type
     */
    public static function getText($string, $default) {
        if ($string == '' || !$string) {
            return $default;
        }
        return filter_var($string, FILTER_SANITIZE_MAGIC_QUOTES);
    }

    /**
     * allow raw value without validation
     * @param type $string
     * @param type $default
     * @return type
     */
    public static function getRaw($string, $default) {
        if ($string == '' || !$string) {
            return $default;
        }
        return $string;
    }

}
