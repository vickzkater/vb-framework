<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

class JBUser{

	public $id;
	public $fullname;
	public $email;
	public $level;
	public $grouprules;
	public $password;
	public $registered;
	public $language;

	public static $instance = null;

	function __construct($id=null){

		$db = App::getDbo();
		$cfg = App::getConfig();

		$session = App::getSession();
		$sessUserid = $session->get('userid');

		$reqid = $id;

		if(!$id){
			if($sessUserid){
				$id = $sessUserid;
			}else{
				$id = 0;
			}
		}

		if($id < 1){
			$this->id = 0;
			$this->level = array();
			$this->grouprules = array();
			$this->fullname = 'Guest';
			$this->registered = null;
			$this->language = null;
			self::$instance = $this;
			return false;
		}

		$sql = 'SELECT a.*, GROUP_CONCAT(distinct b.level separator \',\') as level, GROUP_CONCAT(distinct d.name separator \',\') as grouprules
						FROM users a
						LEFT JOIN user_usergroup b on a.id = b.userid
						LEFT JOIN usergroups f on b.level = f.id
						LEFT JOIN usergroups g on f.path like concat(g.path,\'%\')
						LEFT JOIN groups_rules c on g.id = c.groupid
						LEFT JOIN rules d on c.ruleid = d.id
						where a.id = '.(int)$id.' and (d.state != 0 or d.state is null)
						GROUP BY a.id';

		$db->setQuery($sql);
		$results = $db->loadObjectList();
		if(count($results) > 0){
			foreach ($results as $a)
			{
				$this->id = $a->id;
				$this->fullname = $a->fullname;
				$this->email = $a->email;
				$this->level = explode(",",$a->level);
				$this->grouprules = explode(",",$a->grouprules);
				$this->password = $a->password;
				$this->language = $a->language;
				$this->registered = $a->tglaktif;
			}
		}else{
			$this->id = 0;
			$this->level = array();
			$this->grouprules = array();
			$this->fullname = 'Guest';
			$this->registered = null;
			$this->language = null;
		}

		if(!$reqid){
			self::$instance = $this;
		}
	}

	public static function getInstance($id = null)
	{
		//just call this from child
		if(!$id)
		{
			if(!self::$instance){
				$user = new JBUser();
				self::$instance = $user;
				return $user;
			}else{
				return self::$instance;
			}
		}else{
			return new JBUser($id);
		}
	}

	public function refresh()
	{
		$session = App::getSession();
		$sessUserid = $session->get('userid');

		self::$instance = new JBUser();
	}

	public function authorize($string)
	{
		/*
		 * cek apakah ada rule yang diminta di array usergroup rule
		 */
		if(in_array($string, $this->grouprules))
		{
			return true;
		}else{
			//superadmin selalu bisa..
			$cfg = App::getConfig();
			if(in_array($cfg->superadmin, $this->level))
			{
				return true;
			}else{
				return false;
			}
		}
	}

	public function prohibitGuest()
	{
		if($this->id < 1)
		{
			App::redirect();
		}
	}

	public function getUsergroup()
	{
		$ug = array();
		$db = App::getDbo();
		$sql = 'SELECT * FROM user_usergroup where userid = '.(int)$this->id;
		$db->setQuery($sql);
		$results = $db->loadObjectList();
		if(count($results) > 0)
		{
			foreach($results as $r)
			{
				$ug[] = $r->level;
			}
		}
		return $ug;
	}

	public static function getAllUsergroup()
	{
		$db = App::getDbo();
		$sql = 'SELECT * FROM usergroups';
		$db->setQuery($sql);
		$results = $db->loadObjectList();
		return $results;
	}

	public function newUsergroup()
	{
		//query nya seperti ini mungkin..
		$sql = "update usergroups set path = replace(path,'1/3/','1/2/3/') where path like '1/3/%'";
	}

	//usergroup is array
	public function saveUsergroup($usergroup)
	{
		$db = App::getDbo();
		$sql = "DELETE FROM user_usergroup where userid = ".(int)$this->id;
		$db->setQuery($sql);

		foreach($usergroup as $k => $v)
		{
			$obj = new stdClass();
			$obj->userid = (int)$this->id;
			$obj->level = (int)$v;
			$db->insertObject('user_usergroup', $obj, 'userid');
		}
	}

	public static function hashPassword($password)
	{
		return strtoupper(substr(md5($password), 0, 23));
	}
}

?>
