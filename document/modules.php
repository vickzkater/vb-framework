<?php
/* IMS CMS
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

abstract class IModules {
	
	abstract public function getHtml();
	
	static $headscript = array();
	static $foot = array();
	
	public $title;
	public $name;
	public $note;
	public $pages;
	public $params;	
	public $position;

	public static function getAll()
	{
		$modules = array();
        $db = App::getDbo();
        $sql = 'select * from modules where active = 1 order by ord asc';
        $db->setQuery($sql);
        $result = $db->loadObjectList();
        if(count($result)>0){
            foreach($result as $a)
            {
				$obj = new stdClass();
				$obj->id = $a->id;
				$obj->title = $a->title;
				$obj->name = $a->name;
				$obj->note = $a->note;
				$obj->pages = explode(",",$a->pages);
				$obj->params = json_decode($a->params);
				$obj->position = $a->position;
                $modules[$a->position][] = $obj;
            }
        }
		return $modules;
	}
	
	public function addScript($js, $header = false)
	{	
		$js = '<script src="'.$js.'"></script>'."\n";
		if($header){
			self::$headscript[$this->position][] = $js;
		}else{
			self::$foot[$this->position][] = $js;
		}
	}
	
	public function addInlineScript($js, $header = false)
	{	
		if($header){
			self::$headscript[$this->position][] = $js;
		}else{
			self::$foot[$this->position][] = $js;
		}
	}
	
	public function getLayout($filename, $var = array())
	{
		$cfg = App::getConfig();
		$path = dirname(debug_backtrace()[0]['file'])."/tmpl/".$filename.".php";
		
		$ovr = JBAPPS."/templates/".$cfg->template."/html/mod_".$this->name."/".$filename.".php";
		
		ob_start();
		
		//adding CodeIgniter add variable to layout
		if(count($var) > 0)
		{
			foreach($var as $key => $value) {
			   $$key = $value;
			}			
		}		
	
		// Muat halaman layout
		if(file_exists($ovr))
		{
			include $ovr;			
		}else{
			include $path;
		}
		
		$output = ob_get_clean();		
		
		return $output;		
	}

	public function getModel($name)
    {
		$option = $name;
		$modelPath = JBCOMPONENT.'/'.$name.'/models/'.$name.'.php';
		
		if(file_exists($modelPath)){
			require_once $modelPath;
			$model = ucfirst($option).'Models'.ucfirst($name);
			if(class_exists($model))
			{
				return new $model;
			}else{
				App::message('There is an error in getModel, please report to Administrator', 'error');
				return false;				
			}
		}else{
			App::message('There is an error in getModel, please report to Administrator, ', 'error');
			return false;			
		}
    }
}