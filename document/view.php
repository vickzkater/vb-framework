<?php

/* VB Framework
  (C) Vicky Budiman */
// No direct access to this file
defined('direct') or die;

class JBView {

    public $model;
    public $layout;
    public $layoutPath;
    public $sysmsg = array();
    public static $meta = array();
    public static $headscript = array();
    public static $foot = array();
    public static $title = 'User Panel';
    public static $divider = '-';
    public static $modules = null;
    public static $modhtml = null;
    public static $modheadscript = array();
    public static $modfoot = array();

    function __construct($model, $layoutpath) {
        $page = Jinput::get('page', 0, 'int');
        if ($page == 404) {
            $this->fourohfour();
        }
        $this->model = $model;
        $this->layoutPath = $layoutpath;
    }

    protected function getMessage() {
        $html = '';
        $messages = App::getMessage();
        if (is_array($messages)) {
            foreach ($messages as $mes) {
                $this->sysmsg[] = (object) $mes;
            }
        }
        App::clearMessage();
        return $html;
    }

    public function getModel($name) {
        $option = Jinput::get('option', null, 'word');
        $modelPath = JBCOMPONENT . '/' . $option . '/models/' . $name . '.php';

        if (file_exists($modelPath)) {
            require_once $modelPath;
            $model = ucfirst($option) . 'Models' . ucfirst($name);
            if (class_exists($model)) {
                return new $model;
            } else {
                App::message('There is an error in getModel, please report to Administrator', 'error');
                return false;
            }
        } else {
            App::message('There is an error in getModel, please report to Administrator, ', 'error');
            return false;
        }
    }

    public function addStylesheet($css, $external = false) {
        if (!$external) {
            $css = JBASE . $css;
        }
        $style = '<link rel="stylesheet" href="' . $css . '">' . "\n";
        self::$headscript[] = $style;
    }

    public function addScript($js, $header = false) {
        $js = '<script type="text/javascript" src="' . $js . '"></script>' . "\n";
        if ($header) {
            self::$headscript[] = $js;
        } else {
            self::$foot[] = $js;
        }
    }

    public function addInlineScript($js, $header = false) {
        if ($header) {
            self::$headscript[] = $js;
        } else {
            self::$foot[] = $js;
        }
    }

    public function setFooter($script) {
        self::$foot[] = $script;
    }

    public function setMetaData($name, $value, $equiv = 'name') {
        $data = new stdClass();
        $data->name = $name;
        $data->value = $value;
        $data->equiv = $equiv;
        self::$meta[] = $data;
    }

    public function setTitle($title) {
        self::$title = $title;
    }

    public function getHeader() {
        $output = '';

        if (count(self::$meta) > 0) {
            foreach (self::$meta as $m) {
                if ($m->equiv == 'tag') {
                    $output .= '<' . $m->name . '>' . $m->value . '</' . $m->name . '>' . "\n";
                } elseif ($m->equiv == 'equiv') {
                    $output .= '<meta http-equiv="' . $m->name . '" content="' . $m->value . '">' . "\n";
                } else {
                    $output .= '<meta name="' . $m->name . '" content="' . $m->value . '">' . "\n";
                }
            }
        }

        if (count(self::$headscript) > 0) {
            foreach (self::$headscript as $k => $v) {
                $output .= $v;
            }
        }

        $output .= '';
        //reset $meta and $headscript after done
        self::$meta = array();
        self::$headscript = array();
        return $output;
    }

    public function getFooter() {
        $output = '';
        if (count(self::$foot) > 0) {
            foreach (self::$foot as $k => $v) {
                $output .= $v;
            }
        }
        //reset $foot after done
        self::$foot = array();
        return $output;
    }

    public function getComponent() {
        $cfg = App::getConfig();
        // Mulai output Buffer
        ob_start();
        if (!file_exists($this->layoutPath)) {
            $path = JBAPPS . "/templates/" . $cfg->template . "/404.php";
            if (!file_exists($path)) {
                echo '<h3>View Not Found</h3>';
            } else {
                include $path;
            }
        } else {
            //cek apakah ada override dari template.. since 20170303
            $com = Jinput::get("option", null, "string");
            $view = Jinput::get("view", null, "string");
            $layout = Jinput::get("layout", null, "string");

            $ovr = JBAPPS . "/templates/" . $cfg->template . "/html/" . $com . "/" . $view . "/" . $layout . ".php";
            // Muat halaman layout
            if (file_exists($ovr)) {
                include $ovr;
            } else {
                include $this->layoutPath;
            }
        }
        // get halaman contentnya
        $this->layout = ob_get_clean();

        return $this->layout;
    }

    private function getTemplate() {
        // Mulai output Buffer
        ob_start();

        $cfg = App::getConfig();

        $temp = Jinput::get('tpl', $cfg->template, 'string');
        $path = JBAPPS . "/templates/" . $temp . "/index.php";

        if (file_exists($path)) {
            include $path;
        } else {
            $path = JBAPPS . "/templates/" . $cfg->template . "/index.php";
            // Muat halaman template
            include $path;
        }

        // get halaman contentnya
        $output = ob_get_clean();

        return $output;
    }

    private function initModules() {
        $cfg = App::getConfig();
        $option = Jinput::get('option', $cfg->component, 'string');
        $view = Jinput::get('view', $cfg->view, 'string');
        $layout = Jinput::get('layout', 'default', 'string');

        $page = $option . "." . $view . "." . $layout;

        if (!self::$modules) {
            self::$modules = IModules::getAll();
        }

        if (!self::$modhtml) {
            self::$modhtml = array();
        }


        foreach (self::$modules as $position => $object) {
            $mod = null;
            foreach ($object as $o) {
                if ($o->note == 'all') {
                    $path = JBAPPS . "/modules/" . $o->name . "/" . $o->name . ".php";
                    if (file_exists($path)) {
                        require_once $path;
                        $class = ucfirst($o->name) . "Module";
                        if (class_exists($class)) {
                            $module = new $class();
                            $module->title = $o->title;
                            $module->name = $o->name;
                            $module->note = $o->note;
                            $module->pages = $o->pages;
                            $module->params = $o->params;
                            $module->position = $o->position;

                            $mod .= $module->getHtml();
                        } else {
                            continue;
                        }
                    }
                }

                if ($o->note == 'except' && !in_array($page, $o->pages)) {
                    $path = JBAPPS . "/modules/" . $o->name . "/" . $o->name . ".php";
                    if (file_exists($path)) {
                        require_once $path;
                        $class = ucfirst($o->name) . "Module";
                        if (class_exists($class)) {
                            $module = new $class();
                            $module->title = $o->title;
                            $module->name = $o->name;
                            $module->note = $o->note;
                            $module->pages = $o->pages;
                            $module->params = $o->params;
                            $module->position = $o->position;
                            $mod .= $module->getHtml();
                        } else {
                            continue;
                        }
                    }
                }

                if ($o->note == 'in' && in_array($page, $o->pages)) {
                    $path = JBAPPS . "/modules/" . $o->name . "/" . $o->name . ".php";
                    if (file_exists($path)) {
                        require_once $path;
                        $class = ucfirst($o->name) . "Module";
                        if (class_exists($class)) {
                            $module = new $class();
                            $module->title = $o->title;
                            $module->name = $o->name;
                            $module->note = $o->note;
                            $module->pages = $o->pages;
                            $module->params = $o->params;
                            $module->position = $o->position;

                            $mod .= $module->getHtml();
                        } else {
                            continue;
                        }
                    }
                }
            }
            if ($mod != null) {
                self::$modhtml[$position] = $mod;
            }
        }
    }

    private function countModules($position) {
        if (isset(self::$modhtml[$position])) {
            return true;
        } else {
            return false;
        }
    }

    private function getModules($template) {
        foreach (self::$modhtml as $position => $html) {
            $count = 0;
            $template = str_replace("<embed=\"module\" pos=\"" . $position . "\">", $html, $template, $count);
            if ($count > 0) {
                if (isset(IModules::$headscript[$position])) {
                    if (count(IModules::$headscript[$position]) > 0) {
                        foreach (IModules::$headscript[$position] as $k => $v) {
                            $this->addInlineScript($v, true);
                        }
                    }
                }

                if (isset(IModules::$foot[$position])) {
                    if (count(IModules::$foot[$position]) > 0) {
                        foreach (IModules::$foot[$position] as $k => $v) {
                            $this->addInlineScript($v);
                        }
                    }
                }
            }
        }
        return $template;
    }

    private function parseHtml() {
        $cfg = App::getConfig();
        //set sitename
        $sitename = self::$title;
        if(isset($cfg->sitename)){
            $sitename = $cfg->sitename;
        }
        //set divider of sitename
        $divider = self::$divider;
        if(isset($cfg->divider)){
            $divider = $cfg->divider;
        }
        
        //init modules
        $this->initModules();
        //parse component
        $comp = $this->getComponent();
        //parse module inside component since 02/02/2017 jiboy
        $comp = $this->getModules($comp);
        //parse header
        $this->setMetaData('title', $sitename, 'tag');
        $bhead = $this->getHeader();
        //parse footer
        $bfoot = $this->getFooter();
        //parse message
        $msg = $this->getMessage();
        //parse template	
        $output = $this->getTemplate();
        //parse module
        $output = $this->getModules($output);
        //check one more time after template parsing
        //parse header
        $ahead = $this->getHeader();
        //parse footer
        $afoot = $this->getFooter();
        //combine header
        $head = $bhead . "\n" . $ahead;
        //combine footer
        $foot = $bfoot . "\n" . $afoot;
        $output = str_replace("<embed=\"header\">", $head, $output);
        $output = str_replace("<embed=\"component\">", $comp, $output);
        $output = str_replace("<embed=\"footer\">", $foot, $output);
        $output = str_replace("<embed=\"system-message\">", $msg, $output);
        $output = preg_replace('/(<title>)([^<]|<.+>.*<\/.+>)+(<\/title>)/i', '$1' . $sitename . ' ' . $divider . ' ' . self::$title . '$3', $output);

        return $output;
    }

    public function limitHtml($option = array(), $name = 'limit', $id = 'limit', $class = 'limit') {
        $limit = Jinput::get('limit', 0, 'int');
        if (count($option) < 1) {
            $option["5"] = 5;
            $option["30"] = 30;
            $option["50"] = 50;
            $option["100"] = 100;
            $option["0"] = "All";
        }
        $html = '<div class="pagination-limit">
					<label style="display:inline;">Show limit : </label>
					<select name="' . $name . '" id="' . $id . '" class="' . $class . '">';

        foreach ($option as $k => $v) {
            $selected = '';
            if ($limit == $k) {
                $selected = 'selected=Selected';
            }
            $html .= '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
        }

        $html .= '</select></div>';

        return $html;
    }

    public function render() {
        //call plugin onBeforeViewRender
        $plugin = IPluginHelper::getInstance('view');
        $results = $plugin->trigger('onBeforeViewRender');

        $tmp = $this->parseHtml();

        //call plugin onAfterViewRender
        $plugin = IPluginHelper::getInstance('view');
        $results = $plugin->trigger('onAfterViewRender');

        return $tmp;
    }

    private function sub($page) {
        $output = '';

        $option = Jinput::get('option', null, 'string'); //office
        $view = Jinput::get('view', null, 'string'); //user
        $layout = Jinput::get('layout', 'default', 'string'); //group
        $path = JBAPPS . "/components/" . $option . "/views/" . $view . "/tmpl/" . $layout . "." . $page . ".php";

        if (file_exists($path)) {
            ob_start();

            include $path;

            $output = ob_get_clean();
        }

        return $output;
    }

    /**
     * Method to escape output.
     *
     */
    public function escape($output) {
        // Escape the output.
        return htmlspecialchars($output, ENT_COMPAT, 'UTF-8');
    }

    public function fourohfour() {
        $cfg = App::getConfig();
        // Mulai output Buffer
        ob_start();
        $path = JBAPPS . "/templates/" . $cfg->template . "/404.php";
        if (file_exists($path)) {
            include $path;

            // get halaman contentnya
            $this->layout = ob_get_clean();

            $this->layout = html_entity_decode($this->layout);
        } else {
            $this->layout = '404 Error';
        }

        echo $this->layout;
        die;
    }

}

class JBViewRaw {

    public $model;
    public $layoutpath;
    public $layout;

    function __construct($model, $layoutpath) {
        $this->model = $model;
        $this->layoutPath = $layoutpath;
    }

    public function getComponent() {
        // Mulai output Buffer
        ob_start();
        if (!file_exists($this->layoutPath)) {
            echo 'Requested layout not found';
        } else {
            // Muat halaman layout
            include $this->layoutPath;
        }

        // get halaman contentnya
        $this->layout = ob_get_clean();

        $this->layout = html_entity_decode($this->layout);
        return $this->layout;
    }

    public function render() {
        //call plugin onBeforeViewRender
        $plugin = IPluginHelper::getInstance('view');
        $results = $plugin->trigger('onBeforeViewRender');

        $tmp = $this->getComponent();

        //call plugin onAfterViewRender
        $plugin = IPluginHelper::getInstance('view');
        $results = $plugin->trigger('onAfterViewRender');
        return $tmp;
    }

}

class JBViewPjax {

    public $model;
    public $layoutPath;
    public $layout;
    public $sysmsg = 0;
    public $type;
    public static $title = null;
    public static $headscript = array();
    public static $foot = array();

    function __construct($model, $layoutpath) {
        $this->model = $model;
        $this->layoutPath = $layoutpath;
    }

    public function setTitle($title) {
        self::$title = $title;
    }

    protected function getMessage() {
        $html = '';
        $messages = App::getMessage();
        if (is_array($messages)) {
            foreach ($messages as $mes) {
                $alert = '';

                if (isset($mes['type'])) {
                    $alert = 'alert-' . $mes['type'];
                }
                $html .= '<div class="alert alert-block ' . $alert . '">
							<p>' . $mes['message'] . '</p>
							</div>';
                $this->sysmsg = $this->sysmsg + 1;
            }
        }
        App::clearMessage();
        return $html;
    }

    public function addStylesheet($css, $external = false) {
        if (!$external) {
            $css = JBASE . $css;
        }
        $style = '<link rel="stylesheet" href="' . $css . '">' . "\n";
        self::$headscript[] = $style;
    }

    public function addScript($js, $header = false) {
        if ($header) {
            self::$headscript[] = $js;
        } else {
            self::$foot[] = $js;
        }
    }

    public function addInlineScript($js, $header = false) {
        if ($header) {
            self::$headscript[] = $js;
        } else {
            self::$foot[] = $js;
        }
    }

    public function getHeader() {
        $output = '';

        if (count(self::$headscript) > 0) {
            foreach (self::$headscript as $k => $v) {
                $output .= $v;
            }
        }

        $output .= '';
        //reset $headscript after done
        self::$headscript = array();
        return $output;
    }

    public function getFooter() {
        $output = '';
        if (count(self::$foot) > 0) {
            foreach (self::$foot as $k => $v) {
                $output .= $v;
            }
        }
        //reset $foot after done
        self::$foot = array();
        return $output;
    }

    private function getTemplate() {
        $output = '<embed="header"><embed="component"><embed="footer">';

        return $output;
    }

    private function getComponent() {
        $output = '';
        if ($this->layoutPath) {
            // Mulai output Buffer
            ob_start();
            if (!file_exists($this->layoutPath)) {
                echo '<h3>Layout Path Not Found</h3>';
            } else {
                // Muat halaman layout
                include $this->layoutPath;
            }

            // get halaman contentnya
            $output = ob_get_clean();
        }

        return $output;
    }

    protected function parseHtml() {
        $cfg = App::getConfig();

        $head = $this->getHeader();
        //parse component
        $comp = $this->getComponent();
        $foot = $this->getFooter();
        //parse template	
        $output = $this->getTemplate();

        $output = str_replace("<embed=\"header\">", $head, $output);
        $output = str_replace("<embed=\"component\">", $comp, $output);
        $output = str_replace("<embed=\"footer\">", $foot, $output);

        return $output;
    }

    public function getModel($name) {
        $option = Jinput::get('option', null, 'word');
        $modelPath = JBCOMPONENT . '/' . $option . '/models/' . $name . '.php';

        if (file_exists($modelPath)) {
            require_once $modelPath;
            $model = ucfirst($option) . 'Models' . ucfirst($name);
            if (class_exists($model)) {
                return new $model;
            } else {
                App::message('There is an error please report to Administrator', 'error');
                return false;
            }
        } else {
            App::message('There is an error please report to Administrator, ', 'error');
            return false;
        }
    }

    public function limitHtml($option = array(), $name = 'limit', $id = 'limit', $class = 'limit') {
        $limit = Jinput::get('limit', 0, 'int');
        if (count($option) < 1) {
            $option["5"] = 5;
            $option["30"] = 30;
            $option["50"] = 50;
            $option["100"] = 100;
            $option["0"] = "All";
        }
        $html = '<div class="pagination-limit">
					<label style="display:inline;">Show limit : </label>
					<select name="' . $name . '" id="' . $id . '" class="' . $class . '">';

        foreach ($option as $k => $v) {
            $selected = '';
            if ($limit == $k) {
                $selected = 'selected=Selected';
            }
            $html .= '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
        }

        $html .= '</select></div>';

        return $html;
    }

    public function render() {
        //call plugin onBeforeViewRender
        $plugin = IPluginHelper::getInstance('view');
        $results = $plugin->trigger('onBeforeViewRender');

        $cfg = App::getConfig();

        $output = array();
        $output['html'] = $this->parseHtml();

        if (self::$title) {
            $output['title'] = $cfg->sitename . ' - ' . self::$title;
        }

        $message = $this->getMessage();

        if ($this->sysmsg > 0) {
            $output['message'] = $message;
        }
        if ($this->type = 'json') {
            header('Content-Type: application/json');
        }
        $this->layout = json_encode($output);

        //call plugin onAfterViewRender
        $plugin = IPluginHelper::getInstance('view');
        $results = $plugin->trigger('onAfterViewRender');

        return $this->layout;
    }

}

