<?php
/* VB Framework
 * For managing managing sessions
 * (C) Vicky
 */

// No direct access to this file
defined('direct') or die;

class ISession {

	private $data;

	/**
	 * ISession instances container.
	 *
	 * @var    ISession
	 */
	protected static $instance;

	/**
	 * Returns the global Session object, only creating it
	 * if it doesn't already exist.
	 *
	 * @param   string  $handler  The type of session handler.
	 * @param   array   $options  An array of configuration options.
	 *
	 * @return  ISession  The Session object.
	 */
	public static function getInstance()
	{
		if (!is_object(self::$instance))
		{
			self::$instance = new ISession();
		}

		return self::$instance;
	}
	
    public function set($name, $value)
    {
		/* fixing session not save - 2015-10-14 */
		if(session_status() !== PHP_SESSION_ACTIVE){
			session_start();
		}
		/* end add */
        $_SESSION[$name] = $value;
    }
	
	public function save()
	{
		/*
		 * prevent from locking see http://konrness.com/php5/how-to-prevent-blocking-php-requests/
		 */	
		session_write_close();
		
		//restarting session		
		if(session_status() !== PHP_SESSION_ACTIVE){
			session_start();
		}
	}

    public function get($name)
    {
		if(isset($_SESSION[$name])){
			return $_SESSION[$name];
		}else{
			return null;
		}
    }
	
	public function remove($name)
	{
		unset($_SESSION[$name]);
	}
	
	public function destroy()
	{
		session_unset();
		session_destroy();
		
		$this->register();
		
		session_start();
		session_regenerate_id(true);
	}
	
	public function check()
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$client = $_SERVER['HTTP_USER_AGENT'];
		
		$userIP = $this->get('ipaddress');
		$userClient = $this->get('browser');
		
		if($userIP || $userClient)
		{
			if($ip != $userIP && $userClient != $client)
			{
				$this->destroy();
			}
		}else{
			$this->set('ipaddress', $ip);
			$this->set('browser', $client);
			$this->save();
		}
	}	
	
	public function init()
	{
		$cfg = App::getConfig();
		if($cfg->sessiondatabase == 1){
			// Disable transparent sid support
			ini_set('session.use_trans_sid', '0');

			// Only allow the session ID to come from cookies and nothing else.
			ini_set('session.use_only_cookies', '1');		

			session_name($cfg->sessionname);	

			// Keep session config
			$cookie = session_get_cookie_params();
		
			register_shutdown_function('session_write_close');

			$this->register();

			// Restore config
			session_set_cookie_params($cookie['lifetime'], $cookie['path'], $cookie['domain'], $cookie['secure'], true);				
		}
		session_start();
		
		$this->check();
		return true;
	}
	
	public function restart()
	{
		$this->data = $_SESSION;
		
		if(session_status() === PHP_SESSION_ACTIVE){
			session_unset();
			session_destroy();
		}
		
		$this->register();
		
		session_regenerate_id(true);
		
		$_SESSION = $this->data;
		
		session_write_close();
		session_start();
	}
	
	public function register()
	{
		$session = new JBSessionStorage();

		session_set_save_handler(array($session,"open"),
								 array($session,"close"),
								 array($session,"read"),
								 array($session,"write"),
								 array($session,"destroy"),
								 array($session,"gc"));
		return true;
	}
	
	public static function getToken($print = 'html')
	{
		$ses = self::getInstance();
		$formkey = $ses->get('formkey');
		if(!$formkey)
		{
			$ses->set('formkey', md5(time().'777'));
			$ses->save();
		}
		
		$token = $ses->get('formkey');
		
		if($print == 'html'){
			$html = '<input type="hidden" name="formkey" value="'.$token.'">';
			return $html;
		}
		
		if($print == '')
		{
			return $token;
		}
	}
	
	public static function checkToken()
	{
		$formkey = Jinput::post('formkey', null, 'string');
		$ses = self::getInstance();
		$sesformkey = $ses->get('formkey');

		if($sesformkey)
		{
			if($formkey != $sesformkey)
			{
				App::message('Invalid token please try again <a onclick="window.location.href = window.location.href;" href="#">click here to reload</a>', 'error');
				return false;
			}
			return true;
		}
		App::message('Invalid token please try again <a onclick="window.location.href = window.location.href;" href="#">click here to reload</a>', 'error');
		return false;
	}

}
