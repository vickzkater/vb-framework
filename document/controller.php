<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

class JBControllers{

    public function getModel($name)
    {
		$option = Jinput::get('option', null, 'word');
		$modelPath = JBCOMPONENT.'/'.$option.'/models/'.$name.'.php';
		
		if(file_exists($modelPath)){
			require_once $modelPath;
			$model = ucfirst($option).'Models'.ucfirst($name);
			if(class_exists($model))
			{
				return new $model;
			}else{
				App::message('There is an error please report to Administrator', 'error');
				return false;				
			}
		}else{
			App::message('There is an error please report to Administrator, ', 'error');
			return false;			
		}
    }
	
	protected function fourohfour()
	{
		App::fourohfour();
	}
}

?>