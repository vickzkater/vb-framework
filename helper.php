<?php

/* Helper Class | VB Framework
  (C) Vicky */
// No direct access to this file
defined('direct') or die;

class Helper {

    /**
     * to dumping variable
     * @param anytype $var (variable want to dumped)
     */
    public static function dump($var) {
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }

    /**
     * to validate and get value variable from insecure sources, such as user input, server, cookie
     * @param string $input (source name)
     * @param string $var (variable name)
     * @return string (value of inputted variable)
     */
    public static function getValue($input, $var) {
        if ($input && $var) {
            switch ($input) {
                case 'get':
                    return filter_input(INPUT_GET, $var);
                case 'post':
                    return filter_input(INPUT_POST, $var);
                case 'cookie':
                    return filter_input(INPUT_COOKIE, $var);
                case 'server':
                    return filter_input(INPUT_SERVER, $var);
                default:
                    return 'Source is unknown';
            }
        } else {
            return '';
        }
    }

    /**
     * to get IP of user
     * @return string (user IP)
     */
    public static function getIP() {
        return $this->getValue('server', 'REMOTE_ADDR');
    }

    /**
     * to get the passed time (show bla..bla.. ago)
     * @param int $timestamp
     * @return string (time ago)
     */
    public static function timeAgo($timestamp) {
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        $difference = time() - $timestamp;

        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }

        $dif = round($difference);

        if ($dif != 1) {
            $periods[$j] .= "s";
        }

        return $dif . " " . $periods[$j] . " ago";
    }

    /**
     * for debugging purpose
     */
    public static function debugging() {
        echo "<br><br><pre>*Time Elapsed: <b id='elapsed-debug'>" . (microtime(true) - App::getTimeStart()) . "</b>s on <b>" . count(IDatabase::$allquery) . "</b> query(s) \n";
        $i = 1;
        foreach (IDatabase::$allquery as $d) {
            echo $i++ . ") " . $d . "\n";
        }
        echo '</pre>';

        // display cookies, sessions, and included files
        $values = ['Cookies' => filter_input_array(INPUT_COOKIE), 'Sessions' => $_SESSION, 'Included Files' => get_included_files()];
        foreach ($values as $key => $val) {
            echo "<pre><b>*" . $key . " - Total (" . count($val) . ")</b><br>";
            $i = 1;
            foreach ($val as $k => $v) {
                if ($i < 10) {
                    $no = "&nbsp;" . $i;
                }else{
                    $no = $i;
                }
                echo $no . ') ';
                if (!is_numeric($k)) {
                    echo $k . ' = ';
                }
                echo $v . '<br>';
                $i++;
            }
            echo "</pre>";
        }
    }

    /**
     * to encrypt-decrypt string using cryptography symmetric
     * @param string $str_pass (plaintext)
     * @return string (ciphertext)
     */
    public static function acak($str_pass) {
        $len_str_pass = strlen($str_pass);
        $str_encrypted_pass = "";
        for ($position = 0; $position < $len_str_pass; $position++) {
            $key_to_use = (255 + $len_str_pass + $position + 1) % 255;
            $byte_to_be_encrypted = substr($str_pass, $position, 1);
            $ascii_num_byte_to_encrypt = ord($byte_to_be_encrypted);
            $xored_byte = $ascii_num_byte_to_encrypt ^ $key_to_use;
            $encrypted_byte = chr($xored_byte);
            $str_encrypted_pass .= $encrypted_byte;
        }
        return $str_encrypted_pass;
    }

    /**
     * to encode plain URL using base64
     * @param string $_input (plain URL)
     * @return string (cipher URL)
     */
    public static function base64UrlEncode($_input) {
        return str_replace(array('=', '+', '/'), array('_', '-', ','), base64_encode($_input));
    }

    /**
     * to decode cipher URL using base64
     * @param string $_input (cipher URL)
     * @return string (plain URL)
     */
    public static function base64UrlDecode($_input) {
        return base64_decode(str_replace(array('_', '-', ','), array('=', '+', '/'), $_input));
    }

    /**
     * to encrypt URL using salting, function acak and base64UrlEncode
     * @param string $url (plain URL)
     * @return string (cipher URL)
     */
    public static function UrlEncrypt($url) {
        if ($url) {
            $url = self::acak($url . "jb77");
            return self::base64UrlEncode($url);
        } else {
            return '';
        }
    }

    /**
     * to decrypt URL using salting, function acak and base64UrlDecode
     * @param string $url (cipher URL)
     * @return string (plain URL)
     */
    public static function UrlDecrypt($url) {
        $key = "jb77"; // salt
        $lenkey = (int) strlen($key);
        $purl = self::acak(self::base64UrlDecode($url));
        if (substr($purl, -$lenkey) != $key) {
            return null;
        } else {
            return substr($purl, 0, -$lenkey);
        }
    }

    /**
     * to validate required data
     * @param type $data
     * @param string $elementname
     * @param object $languange
     * @return boolean|type
     */
    public static function requiredData($data, $elementname, $languange) {
        if (!$data) {
            App::message($elementname . ' ' . $languange->_("must_filled"), 'error');
            return false;
        }
        return $data;
    }

    /**
     * to get from_date, to_date, and fixed format of daterange
     * @param string $daterange (sample: August 1, 2018 - August 31, 2018 (GMT+7))
     * @return boolean|\object (daterange, from, to)
     */
    public static function getDaterange($daterange) {
        if ($daterange) {
            // set var object for results
            $results = new stdClass();
            $results->daterange = $daterange;
            // get daterange
            $a = strpos($daterange, " (");
            $dateonly = substr($daterange, 0, $a);
            $date = explode(" - ", $dateonly);
            $results->from = date('m/d/Y', strtotime($date[0]));
            $results->to = date('m/d/Y', strtotime($date[1]));

            // get timezone/GMT
            $b = substr($daterange, $a);
            $c = substr($b, 5);
            $z = strpos($c, ")");
            $gmt = (int) substr($c, 0, $z);
            $results->gmt = $gmt;

            // set GMT+, if GMT > 0
            if ($gmt > 0) {
                $results->daterange = $dateonly . ' (GMT+' . $gmt . ')';
            }
            return $results;
        }
        return false;
    }

}
