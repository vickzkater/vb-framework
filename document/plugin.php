<?php
/* IMS CMS
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

class IPluginHelper {

	private $group = null;
	
	function __construct($group)
	{
		$this->group = $group; 
	}

	public static function getInstance($group)
	{
		return new IPluginHelper($group);
	}
	
	public function trigger($function, $args = array())
	{
		$results = array();
		
		if($this->group){
		
			$path = JBPLUGIN.'/'.$this->group;
			$folders = App::getAllfolder($path);
		
			if(count($folders) > 0){
			
				$obj = null;
				
				$listInstance = array();
			
				foreach($folders as $k => $v){
				
					$file = $path.'/'.$v.'/'.$v.'.php';
				
					//check file exist or not
					if(file_exists($file)){
					
						require_once $file;
						$front = ucfirst($this->group);
						$class = $front.'Plugins'.ucfirst($v);
						
						//check class exists or not
						if(class_exists($class))
						{
							$obj = new $class();
							$value = null;
							
							$obj->group = $this->group;
							$obj->name = $v;
							
							// skip bila ga ada property active
							if(!isset($obj->active))
							{
								continue;
							}			
							
							if(method_exists($obj, $function) && $obj->active)
							{	
								$taskobj = new stdClass();
								
								$functionOrder = $function."_order";
								
								$taskobj->order = 1000;
								if(isset($obj->$functionOrder))
								{
									$taskobj->order = $obj->$functionOrder;
								}
								
								$taskobj->instance = $obj;
								$taskobj->method = $function;
								$taskobj->args = $args;
								
								$listInstance[] = $taskobj;
							}
						}
					}
				}
				
				$results = $this->trigger_dotask($listInstance);
			}
		}
		
		return $results;
	}
	
	private function SortOrder($a, $b)
	{
		return strcmp($a->order, $b->order);
	}
	
	private function trigger_dotask($listInstance)
	{
		$results = array();
		
		if(count($listInstance) > 0)
		{
			usort($listInstance, array($this, "SortOrder"));
			
			foreach($listInstance as $k=>$v)
			{
				/*
				 * using php function call_user_func_array
				 * see http://php.net/manual/en/function.call-user-func-array.php
				 */
				$value = call_user_func_array(array($v->instance, $v->method), $v->args);

				if($value)
				{
					$results[] = $value;
				}				
			}
		}
		
		return $results;
	}

	public function getPlugin($name)
	{
		if($this->group){
		
			$path = JBPLUGIN.'/'.$this->group;
			$folders = App::getAllfolder($path);
		
			if(count($folders) > 0){
			
				$obj = null;
			
				$file = $path.'/'.$name.'/'.$name.'.php';
				
				//check file exist or not
				if(file_exists($file)){
					require_once $file;
					$front = ucfirst($this->group);
					$class = $front.'Plugins'.ucfirst($name);
					
					//check class exists or not
					if(class_exists($class))
					{
						$obj = new $class();
						$obj->group = $this->group;
						$obj->name = $name;
						
						$value = null;
						
						// skip bila ga ada property active
						if(!isset($obj->active))
						{
							//continue;
						}			
						
						if($obj->active)
						{
							return new $obj;
						}
					}
				}
			}
		}
		
		return null;		
	}
}

class IPlugin {

	public $active = false;
	public $order = 1000;
	public $group;
	public $name;
	
	public function addScript($js, $header = false)
	{	
		$js = '<script src="'.$js.'"></script>'."\n";
		if($header){
			JBView::$headscript[] = $js;
		}else{
			JBView::$foot[] = $js;
		}
	}
	
	public function addInlineScript($js, $header = false)
	{	
		if($header){
			JBView::$headscript[] = $js;
		}else{
			JBView::$foot[] = $js;
		}
	}

	public function getLayout($filename, $var = array())
	{
		$cfg = App::getConfig();
		$path = dirname(debug_backtrace()[0]['file'])."/tmpl/".$filename.".php";
		
		$ovr = JBAPPS."/templates/".$cfg->template."/html/plg_".$this->group."_".$this->name."/".$filename.".php";
		
		ob_start();
		
		//adding CodeIgniter add variable to layout
		if(count($var) > 0)
		{
			foreach($var as $key => $value) {
			   $$key = $value;
			}			
		}
	
		// Muat halaman layout
		if(file_exists($ovr))
		{
			include $ovr;			
		}else{
			include $path;
		}
		
		$output = ob_get_clean();		
		
		return $output;		
	}	

}