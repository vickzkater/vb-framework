<?php
/* JoyBoy Framework
(C) Jiboy */
// No direct access to this file
defined('direct') or die;

class Library {

	public static $instance;
	
	public static function getInstance()
	{
		if (!is_object(self::$instance))
		{
			self::$instance = new Library();
		}

		return self::$instance;
	}
	
	public function load($name)
	{
		require_once JBLIBRARY."/".$name."/".$name.".php";
	}
	
	public function getJS($name)
	{
		ob_start();
		
		include JBFRAMEWORK."/js/".$name.".php";
		
        $output = ob_get_clean();
	
        return $output;		
	}

}